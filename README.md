# Out from the box


Un juego del género endless runner, el jugador viaja a través de un túnel 
evitando obstáculos, que en este caso son cubos/cajas, una vez que hay una 
colisión el juego termina y se almacena el puntaje. Puedes volver a iniciar 
con Z. La posición de las cajas/cubos es aleatoria. 

Para moverte utilizar:
+ [ w ] : arriba
+ [ s ]: abajo
+ [ a ] : izquierda
+ [ d ] : derecha

Hay dos versiones, una donde implementamos Q learning y la máquina juega 
y otra para un jugador humano. Ambas se compilan de manera similar. 

## Configuración
`g++ --std=c++11 mew.cpp -o juego -lGL -lGLU -lglut`


## Maintainer
+ Vallejo Gutiérrez Luis Ricardo
+ Feliciano Avelino Ivan Raymundo