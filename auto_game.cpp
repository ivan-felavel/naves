
#include <GL/glut.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstring>
#include <fstream>
#include <bitset>
#include <sstream>

using namespace std;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
/*
Variables globales
*/


void *font = GLUT_BITMAP_TIMES_ROMAN_24;
string defaultMessage = "Aqui va el Score";
string message = defaultMessage;
int METROS = 0;
short CUADRO_ACTUAL = 0;
vector < pair < double, pair<pair<short,bitset<9>>, char>>> TUPLAS;
pair <short, bitset<9>> ESTADO_ACTUAL;
char ACCION_ACTUAL;

GLint CARAS[6][4] = { 
		{0, 1, 2, 3}, {3, 2, 6, 7}, {7, 6, 5, 4},
		{4, 5, 1, 0}, {5, 6, 2, 1}, {7, 4, 0, 3} };

GLfloat COLOR [6][3]={
		{0.0,1.0,0.0},
		{50.50,0.,50.0},
		{1.0,1.0,0.0},
		{0.0,0.0,1.0},
		{1.0,0.0,1.0},
		{0.0,1.0,1.0},
};
float TRASLACION_Z = 0.1;
int flag_score = -1;

vector <bitset<9>> configuraciones;

bool activo=true;


bitset <9> random_config;

double i_l[3];
double j[3];
double k[3];
double v[3];


int EDGE_SIZE = 1;
int AVANCE_Z = 10;

int x_0 = 0;//- EDGE_SIZE - EDGE_SIZE / 2;
int y_0 =  0;//- EDGE_SIZE - EDGE_SIZE / 2;
int z_0 = 0;


GLfloat mov_x = 1.5;
GLfloat mov_y = 1.5;
GLfloat mov_z = 30;

GLfloat vel_pared= 0;
GLfloat vel_tunel= -2;



GLfloat delta = 0.2;
GLfloat delta_vista = 0.1;

GLfloat punto_x = 0;
GLfloat punto_y = 0;
GLfloat punto_z = 0;

GLfloat rota_y = 0;

string first;
string second;
string third;



//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//



struct point { 
	double x, y, z;
	point() { x = y = z = 0.0; }
	point(double _x, double _y,double _z) : x(_x), y(_y), z(_z) {} 
};


struct cubo
{
	GLfloat vertices[8][3];
	vector <point> Pol;
	bool barrera = false;

	void genera_vertices(float x = x_0, float y = y_0, float z = vel_pared)
	{

		Pol.clear();
		Pol.push_back(point(x+1, y+1,z));
		Pol.push_back(point(x+1, y +1+ EDGE_SIZE,z));
		Pol.push_back(point(x+1 + EDGE_SIZE,y +1 + EDGE_SIZE,z));
		Pol.push_back(point(x + EDGE_SIZE+1, y+1,z));

		Pol.push_back(point(x+1, y+1,z+EDGE_SIZE));
		Pol.push_back(point(x+1, y+1 + EDGE_SIZE,z+EDGE_SIZE));
		Pol.push_back(point(x+1 + EDGE_SIZE, y+1 + EDGE_SIZE,z+EDGE_SIZE));
		Pol.push_back(point(x+1 + EDGE_SIZE, y+1,z+EDGE_SIZE));

		vertices[0][0] = vertices[1][0] = vertices[4][0] = vertices[5][0] = x;
		vertices[2][0] = vertices[3][0] = vertices[6][0] = vertices[7][0] = x + EDGE_SIZE;
		vertices[0][1] = vertices[3][1] = vertices[4][1] = vertices[7][1] = y;
		vertices[1][1] = vertices[2][1] = vertices[5][1] = vertices[6][1] = y + EDGE_SIZE;
		vertices[0][2] = vertices[1][2] = vertices[2][2] = vertices[3][2] = z;
		vertices[4][2] = vertices[5][2] = vertices[6][2] = vertices[7][2] = z -EDGE_SIZE;
	}

	void dibuja_barrera()
	{
		srand (time(NULL));	 
		glColor3fv(COLOR[rand() % 5]);
		for (int i = 0; i < 6; i++){
			glBegin(GL_LINE_STRIP);
			glVertex3fv(&vertices[CARAS[i][0]][0]);
			glVertex3fv(&vertices[CARAS[i][1]][0]);
			glVertex3fv(&vertices[CARAS[i][2]][0]);
			glVertex3fv(&vertices[CARAS[i][3]][0]);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3fv(&vertices[CARAS[i][0]][0]);
			glVertex3fv(&vertices[CARAS[i][2]][0]);
			glVertex3fv(&vertices[CARAS[i][1]][0]);
			glVertex3fv(&vertices[CARAS[i][3]][0]);
			glEnd();
		}
		barrera = true;
	}

	vector<point> get_puntos(){
		return Pol;
	}

	bool colision(){
			double p_0[3]={Pol[0].x,Pol[0].y,Pol[0].z};
			double p_1[3]={Pol[1].x,Pol[1].y,Pol[1].z};
			double p_3[3]={Pol[3].x,Pol[3].y,Pol[3].z};
			double p_4[3]={Pol[4].x,Pol[4].y,Pol[4].z};

			double p_c[3]={mov_x,mov_y,15};


		for (int i = 0; i <3 ; ++i)
		{
			///genera
			i_l[i]=p_0[i]-p_4[i];
			j[i]=p_0[i]-p_3[i];
			k[i]=p_0[i]-p_1[i];
			
			v[i]=p_c[i]-p_0[i];
		}
		if(0<=producto_punto(v,i_l)   && producto_punto(v,i_l)<=producto_punto(i_l,i_l))
		if(0<=producto_punto(v,j) && producto_punto(v,j)<=producto_punto(j,j))
			if(0<=producto_punto(v,k)  && producto_punto(v,k)<=producto_punto(k,k)){
				//cout<<"EL punto esta dentro"<<endl;
				return true;
			}

		return false;
	}

	double producto_punto(double a[], double b[]){
		double res=0;
		res=a[0]*b[0]+a[1]*b[1]+a[2]*b[2]; 
		return res;
	}

}PARED[9];


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Funciones 
void punto_vista();
void output(int x, int y, string t_string);
void display_text();
void guarda_score();
void best_scores();
void colision_paredes();
void genera_config_random();
void dibuja_tunel();
void key(unsigned char key, int x, int y);
static void idle(void);
void display(void);
void init(void);
void punto_vista(void);
void genera_posibles_paredes();
void llena_tuplas();
char elegir_mejor_accion(pair <short, bitset<9>>);
void controlar_q(char);
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


GLfloat puntos_medios[9][3]={
	{2.5,2.5,30}, //0
	{1.5,2.5,30}, //1
	{0.5,2.5,30},  //2
	{2.5,1.5,30}, //3
	{1.5,1.5,30}, //4
	{0.5,1.5,30}, //5
	{2.5,0.5,30},//6
	{1.5,0.5,30},//7
	{0.5,0.5,30} //8
};

int cuadrante(){	
	for(int i=0;i<9;i++){
		if( puntos_medios[i][0]== mov_x && puntos_medios[i][1]== mov_y)
			return i;
	}	
}

void llena_tuplas()
{
	freopen("training_complete.in", "r", stdin);
	double q_value;
	short pos;
	bitset <9> wall;
	char accion;
	for (int i = 0; i < 16830; ++i)
	{
		cin >> q_value >> pos >> wall >> accion;
		TUPLAS.push_back(make_pair(q_value, make_pair(make_pair(pos, wall), accion)));
	}
	fclose(stdin);
	
}


char elegir_mejor_accion(pair< short,bitset<9>> estado)
{
	int index = 0;
	float maxi =  -100000;
	for (int i = 0; i < TUPLAS.size(); ++i)
	{	
		if (TUPLAS[i].second.first == estado && TUPLAS[i].first > maxi)
		{
			index = i;
			maxi = TUPLAS[i].first;
		}
	}
	return TUPLAS[index].second.second;
}


void actualiza_q_falla()
{
	for (int i = 0; i < TUPLAS.size(); ++i)
	{	
		if (TUPLAS[i].second.first == ESTADO_ACTUAL && TUPLAS[i].second.second == ACCION_ACTUAL)
		{
			TUPLAS[i].first -= 50;
		}
	}
	ofstream store;
	store.open("training_complete.in", ofstream::out);
	for (int i = 0; i < TUPLAS.size(); ++i)
	{
		store << TUPLAS[i].first << " " << TUPLAS[i].second.first.first << " " << TUPLAS[i].second.first.second << " " << TUPLAS[i].second.second << " " << endl;
	}
	store.close();

}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
void output(int x, int y, string t_string)
{
  int len, i;
  glRasterPos2f(x, y);
  len = t_string.size();
  for (i = 0; i < len; i++) {
	glutBitmapCharacter(font, t_string[i]);
  }
}

void display_text()
{
	message = "Recorrido: ";
	message += to_string(METROS++);
	message += " metros";
	glClear(GL_COLOR_BUFFER_BIT);
	output(-4, 10, message);
}

void guarda_score()
{
	ofstream scoreboard;
	scoreboard.open("scores.txt", ofstream::out | ofstream::app);
	scoreboard << METROS << endl;
	scoreboard.close();
}


void best_scores()
{
	ifstream infile("scores.txt");
	vector <int> v_scores;
	int a;
	while (infile >> a)
		v_scores.push_back(a);
	sort(v_scores.begin(), v_scores.end());
	reverse(v_scores.begin(), v_scores.end());
	first = to_string(v_scores[0]);
	second = to_string(v_scores[1]);
	third = to_string(v_scores[2]);
}

void colision_paredes()
{
	
	for (int i = 0; i < 9; ++i)
	{	
		if(random_config[i])
			if(PARED[i].colision()){
				activo=false;
			}
	}
}

void genera_posibles_paredes()
{
	for (int i = 1; i < 511 ; ++i)
		configuraciones.push_back(i);
}

void genera_config_random()
{
	srand(time(NULL));
	int r = rand() % configuraciones.size() - 1;
	random_config = configuraciones[r];	
}


void dibuja_pared()
{
	PARED[0].genera_vertices(x_0 + EDGE_SIZE * 2, y_0 + EDGE_SIZE * 2, vel_pared);
	PARED[1].genera_vertices(x_0 + EDGE_SIZE, y_0 + EDGE_SIZE * 2, vel_pared);
	PARED[2].genera_vertices(x_0, y_0 + EDGE_SIZE * 2, vel_pared);
	PARED[3].genera_vertices(x_0 + EDGE_SIZE * 2, y_0 + EDGE_SIZE, vel_pared);
	PARED[4].genera_vertices(x_0 + EDGE_SIZE, y_0 + EDGE_SIZE, vel_pared);
	PARED[5].genera_vertices(x_0, y_0 + EDGE_SIZE, vel_pared);
	PARED[6].genera_vertices(x_0 + EDGE_SIZE * 2, y_0 , vel_pared);
	PARED[7].genera_vertices(x_0 + EDGE_SIZE, y_0, vel_pared);
	PARED[8].genera_vertices(x_0,y_0,vel_pared);

	for (int i = 0; i < 9; ++i)
		if (random_config[i]) PARED[i].dibuja_barrera();


	CUADRO_ACTUAL = cuadrante();
	ESTADO_ACTUAL = make_pair(CUADRO_ACTUAL, random_config);
	ACCION_ACTUAL = elegir_mejor_accion(ESTADO_ACTUAL);

	cout << "debo moverme a la : " << ACCION_ACTUAL << endl;

	controlar_q(ACCION_ACTUAL);
}



void dibuja_tunel()
{
	//izq_arriba
	glBegin(GL_LINES);
	glVertex3f(x_0, y_0, 20.0f);
	glVertex3f(x_0, y_0, -20.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(x_0 + 3 * EDGE_SIZE , y_0, 20.0f);
	glVertex3f(x_0 + 3 * EDGE_SIZE, y_0 , - 20.0f);
	glEnd();

	//izquierda

	glBegin(GL_LINES);
	glVertex3f(x_0 , y_0, vel_tunel -30.0f);
	glVertex3f(x_0 , y_0+ 3 * EDGE_SIZE , vel_tunel -30.0f);
	glEnd();
	//derecha
	
	glBegin(GL_LINES);
	glVertex3f(x_0 + 3 * EDGE_SIZE, y_0, vel_tunel -30.0f);
	glVertex3f(x_0 + 3 * EDGE_SIZE, y_0+ 3 * EDGE_SIZE , vel_tunel -30.0f);
	glEnd();


	glBegin(GL_LINES);
	glVertex3f(x_0 + 3 * EDGE_SIZE , y_0 + 3 * EDGE_SIZE,  20.0f);
	glVertex3f(x_0 + 3 * EDGE_SIZE, y_0 + 3 * EDGE_SIZE, - 20.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(x_0, y_0+ 3 * EDGE_SIZE, -20.0f);
	glVertex3f(x_0, y_0 + 3 * EDGE_SIZE, 20.0f);
	glEnd();
}



void key(unsigned char key, int x, int y){  
	switch (key){
		case 'a':
			if(mov_x - delta > 0)
				mov_x=mov_x-delta;
		break;
	
		case 'd':
			if(mov_x + delta < 3)
				mov_x=mov_x+delta;
		break;

		case 'w':
			if(mov_y + delta < 3)
				mov_y=mov_y+delta;
		break;

		case 's':
			if(mov_y - delta > 0)
				mov_y=mov_y-delta;
		break;

		case 'q':
			mov_z=mov_z+delta;
		break;

		case 'e':
			mov_z=mov_z-delta;
		break;

		case 'v':
			cout<<mov_x<<"#"<<mov_y<<"#"<<mov_z<<endl;
		break;

		case 'f':
			rota_y=rota_y+0.1;
		break;

		case 'r':
			mov_x=0;
			mov_y=0;
			mov_z=0;
		break;

		case 'h':
			punto_x=punto_x-delta_vista;
		break;
		case 'k':
			punto_x=punto_x+delta_vista;
		break;
		case 'j':
			punto_y=punto_y-delta_vista;
		break;
		case 'u':
			punto_y=punto_y+delta_vista;
		break;
		case 'y':
			punto_z=punto_z+delta_vista;
		break;
		case 'i':
			punto_z=punto_z-delta_vista;
		break;
		case 'z':
				//vel_pared=vel_pared+delta;
			activo = true;
			vel_pared = 0;
			x_0 = 0;
			y_0 = 0;
			METROS = 0;
			flag_score = -1;
		break;
		case 'x':
		vector<point> v;
		for(int j=0; j<9;j++)
			{ 
				cout<<"Cubo "<<j<<" "<<endl;        
				v=PARED[j].get_puntos();
				for(auto e:v)
					cout<<" "<<e.x<<" "<<e.y << " "<<e.z <<endl ;
			}
		break;
	}
}


void controlar_q(char key){
	switch(key){
		//R, U, L, D, N
		case 'L':
			if(mov_x - 1 > 0)
				mov_x=mov_x-1;
		break;
	
		case 'R':
			if(mov_x + 1 < 3)
				mov_x=mov_x+1;
		break;

		case 'U':
			if(mov_y + 1 < 3)
				mov_y=mov_y+1;
		break;

		case 'D':
			if(mov_y - 1 > 0)
				mov_y=mov_y-1;
		break;

		case 'N':
			//nada
		break;

	}

}


static void idle(void){
		glutPostRedisplay();
}


void display(void){
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//display_text();
	gluLookAt(mov_x,mov_y,15, 
			mov_x, mov_y,0,     
			0, 1, 0.0);
	 
	if(activo){

		if(vel_pared>16){
			 vel_pared = 0;
			 genera_config_random();
		}
		if(vel_tunel>50)
			vel_tunel=0;
		
		display_text();
		//punto_vista();
		dibuja_pared();
		colision_paredes();
		dibuja_tunel();
		glLoadIdentity();
		vel_pared=vel_pared+0.2;
		vel_tunel=vel_tunel+0.2;
		glutSwapBuffers();
	}	

	else if(activo==false && flag_score == -1){

		output(-4, 10, "Tu puntaje:");
		output(-3, 8, to_string(METROS));
		guarda_score();
		actualiza_q_falla();
		flag_score = 1;
		best_scores();
		activo = true;
		vel_pared = 0;
		//x_0 = 0;
		//y_0 = 0;
		METROS = 0;
		flag_score = -1;
		glutSwapBuffers();
	}
	else{
		output(-4, 10, "Tu puntaje:");
		output(-3, 8, to_string(METROS));
		output(-4, 6, "Mejores puntajes:");
		output (-3, 4, first);
		output (-3, 3, second);
		output (-3, 2, third);
		output(-7, 0, "Para comenzar de nuevo presiona Z");
		glutSwapBuffers();
	}
}

void init(void){
		glEnable(GL_DEPTH_TEST);
		glMatrixMode(GL_PROJECTION);
		gluPerspective(  100.0, // apertura del lente de la camara 
		 1.0,
		 1.0,  30.0); // Plano Cerca, Plano Lejano
		glMatrixMode(GL_MODELVIEW);
		
}

void punto_vista(void){
	glPointSize(20.0);
	glBegin(GL_POINTS);
		glColor3f(1, 0,0 );
		glVertex3f(mov_x,mov_y,5);
	glEnd();
}




int main(int argc, char **argv){
	genera_posibles_paredes();
	llena_tuplas();
	glutInit(&argc, argv);
	glutInitWindowSize(660,660); 
	glutInitWindowPosition(100,100);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Cubo");
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(key);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}