#include <bitset>
#include <ctime>
#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;

//Tabla donde se almacenan los Q values con respecto a la acción y estado
struct Store
{
	vector < pair < double, pair<pair<short,bitset<9>>, char>>> Tuplas;
	vector < pair< short,bitset<9> > > estados;
	double Q_inicial = -100;
	string acciones = "NRLUD"; //no muevo, right, left, up, down
 	short posiciones[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
 	
	void genera_Tuplas()
	{
		//generar vector con todas las configuraciones posibles
		vector <bitset<9>> configuraciones;
		for (int i = 1; i < 511 ; ++i)
		{
			bitset <9> config(i);
			configuraciones.push_back(config); 
		}
		//genera todos los estados posibles
		for (short i = 0; i < 9; ++i)
			for (int j = 0; j < configuraciones.size(); ++j)
				estados.push_back(make_pair(i, configuraciones[j]));
		//llena el vector de tuplas
		for (short i = 0; i < 5; ++i)
		{
			char accion = acciones[i];
			for (int j = 0; j < estados.size(); ++j)
			{
				short pos = estados[j].first;
				if (pos == 0 && (accion == 'R' || accion == 'U')) continue;
				if (pos == 1 && (accion == 'U')) continue;
				if (pos == 2 && (accion == 'L' || accion == 'U')) continue;
				if (pos == 3 && (accion == 'R')) continue;
				if (pos == 5 && (accion == 'L')) continue;
				if (pos == 6 && (accion == 'R' || accion == 'D')) continue;
				if (pos == 7 && (accion == 'D')) continue;
				if (pos == 8 && (accion == 'L' || accion == 'D')) continue;
				Tuplas.push_back(make_pair(Q_inicial, make_pair(estados[j], accion)));
			}
		}
	}

	void imprime_tuplas()
	{
		for (int i = 0; i < Tuplas.size(); i++)
			cout << Tuplas[i].first << " " << Tuplas[i].second.first.first << " " << Tuplas[i].second.first.second << " " << Tuplas[i].second.second << " " << endl;
	}
	pair< short,bitset<9> > obtener_estado_random()
	{
		srand (time(NULL));
		int index_estado =  rand() % estados.size() - 1;
		//cout << estados[index_estado].second<< endl;
		return estados[index_estado];
	}
	
	string obtener_acciones(pair< short,bitset<9> > estado)
	{
		short posicion =  estado.first;
		bitset <9> pared = estado.second;
		if (posicion == 0) return "NLD";
		else if (posicion == 1) return "NRLD";
		else if (posicion == 2) return "NRD";
		else if (posicion == 3) return "NLUD";
		else if (posicion == 4) return "NRLUD";
		else if (posicion == 5) return "NRUD";
		else if (posicion == 6) return "NLU";
		else if (posicion == 7) return "NRLU";
		else if (posicion == 8) return "NRU";
	}

	char regresa_una_accion(string acciones)
	{
		srand(time(NULL));
		int r =rand() % acciones.size() - 1;
		char a =  acciones[r];
		return a;
	}

	char elegir_mejor_accion(pair< short,bitset<9>> estado)
	{
		int index = 0;
		float maxi =  -1000000;
		for (int i = 0; i < Tuplas.size(); ++i)
		{	
			if (Tuplas[i].second.first == estado && Tuplas[i].first > maxi)
			{
				index = i;
				maxi = Tuplas[i].first;
			}
		}
		return Tuplas[index].second.second;
	}

	pair <int, pair< short,bitset<9> >> realizarAccion( pair< short,bitset<9>> estado, char accion)
	{
		int recompensa;
		short nueva_pos;
		short pos_actual =  estado.first;
		bitset<9> config_actual =  estado.second;

		pair <short, bitset<9>> nuevo_estado;
		pair <int, pair< short,bitset<9> >> resultado;	 
		
		if (accion == 'U')
			nueva_pos = pos_actual - 3;
		else if (accion == 'D')
			nueva_pos = pos_actual + 3;
		else if (accion == 'R')
			nueva_pos = pos_actual - 1;
		else if (accion == 'L')
			nueva_pos = pos_actual + 1;
		else if (accion == 'N')
			nueva_pos = pos_actual;
		//si hay barrera
		if (config_actual[nueva_pos] == 1)
		{
			recompensa = -100;
			resultado = make_pair(-100, estado);
			return resultado;
		}
		//si no hay barrera
		recompensa = 100;
		nuevo_estado.first = nueva_pos;
		nuevo_estado.second = estado.second;
		resultado = make_pair(recompensa, nuevo_estado);
		return resultado;
	}

	void inicializa_tuplas()
	{
		short nueva_pos = 0;
		short pos_actual =  0;
		bitset<9> config_actual;
		char accion;

		for (int i = 0; i < Tuplas.size(); ++i)
		{	
			config_actual = Tuplas[i].second.first.second;
			pos_actual = Tuplas[i].second.first.first;
			accion = Tuplas[i].second.second;


			if (accion == 'U')
				nueva_pos = pos_actual - 3;
			else if (accion == 'D')
				nueva_pos = pos_actual + 3;
			else if (accion == 'R')
				nueva_pos = pos_actual - 1;
			else if (accion == 'L')
				nueva_pos = pos_actual + 1;
			else if (accion == 'N')
				nueva_pos = pos_actual;
			if (config_actual[nueva_pos] == 1)
				Tuplas[i].first = -100;
			else	
				Tuplas[i].first = 100;

		}
	}
	double obtenerQValue(pair< short,bitset<9>> estado, char accion)
	{
		for (int i = 0; i < Tuplas.size(); ++i)
			if (Tuplas[i].second.first == estado && Tuplas[i].second.second == accion)
			{
				return Tuplas[i].first;
			}
	}

	void guardaQValue(pair< short,bitset<9>> estado, char accion, double Q)
	{

		for (int i = 0; i < Tuplas.size(); ++i)
		{	
			if (Tuplas[i].second.first.first == estado.first && Tuplas[i].second.first.second == estado.second  && Tuplas[i].second.second == accion)
			{
				Tuplas[i].first = Q;
				//cout << Tuplas[i].first << endl;
				return;
			}
		}
	}
}store;


int main ()
{
	store.genera_Tuplas();
	store.inicializa_tuplas();
	double alpha = 0.3; 
	double gamma = 0.75;
	double rho = 0.2;
	double nu = 0.1;
	int iteraciones = 100000000;
	srand(time(NULL));
	time_t start_time = time(NULL);
	time_t end_time;
	time_t seconds = 10800;
	end_time = start_time + seconds;
	pair< short,bitset<9>> estado;
	estado = store.obtener_estado_random(); 



	for (int i = 0; i < iteraciones; i++)
	{
		//Han pasado 3 horas?
		if (time(NULL) > end_time)
			break;
		double r = (double) rand() / (RAND_MAX);

		//Elegimos un nuevo estado?
		if (r < nu)
			estado = store.obtener_estado_random(); 
		
		string acciones_posibles = store.obtener_acciones(estado);
		

		char accion;
		r = (double) rand() / (RAND_MAX);
		
		//Elegir una acción aleatoria?
		

		
		if (r < rho)
			accion = store.regresa_una_accion(acciones_posibles);
		
		//Sino elegir mejor acción
		else
			accion = store.elegir_mejor_accion(estado);
		

		pair <int, pair< short,bitset<9>>> reward_newState;
		int recompensa;
		
		pair< short,bitset<9>> nuevoEstado;
		//realizamos la acción  y retornamos las recompensa y el nuevo estado
		
		reward_newState = store.realizarAccion(estado, accion);
		recompensa = reward_newState.first;
		nuevoEstado = reward_newState.second;
		
		double Q = store.obtenerQValue(estado, accion);

		double maxQ = store.obtenerQValue(nuevoEstado, store.elegir_mejor_accion(nuevoEstado));

		Q = (1 - alpha) * Q + alpha * (recompensa + gamma * maxQ);
		store.guardaQValue(estado, accion, Q);
	}
	store.imprime_tuplas();
}
